# Introduction to Infrastructure as Code (IaC)

## Textbook chapter one

Virtualization / "Software-defined everything" is fantastic, you can
create servers and networks in seconds, so what is the problem? (growth
and change)

"Definition":  
Infrastructure as code is an approach to infrastructure automation based
on practices from software development. It emphasizes consistent,
repeatable routines for provisioning and changing systems and their
configuration. Changes are made to definitions and then rolled out to
systems through unattended processes that include thorough validation.

Configuration drift, Erosion over time

Snowflake servers (pets and cattle)

Automation fear...

Principles of Infrastructure as Code (EMBRACE CHANGE\! Face your
fear\!):

  - Systems Can Be Easily Reproduced

  - Systems Are Disposable

  - Systems Are Consistent (Similarity/Homogeneity)

  - Processes Are Repeatable (Idempotency)

  - Design Is Always Changing

Using Infrastructure as code: self-documented systems\!

Version all things (Traceability, Rollback, Correlation (tags),
Visibility, trigger automatic actions)

Small batch principle

Your systems should be antifragile (beyond robustness)

KEEP IT AS SIMPLE AS YOU CAN\!\!\! (removing code is as valuable as
adding)

## Textbook chapter two

    Dynamic Infrastructure Platform
    ----------------------------+
    SkyHiGh/AWS/Azure/...       | - CLI access 
    - programmable              | - GUI access 
    - on-demand                 | - curl (direct api) 
    - selv-service              |
    (compute, network, storage) |
    ----------------------------+

(Twitter: buy security products based on the API not the console)

IaaS, PaaS, SaaS (DaaS: Data, Desktop, Device, DataCenter, ...)

Where will your data be? (+ private cloud controls your cost maybe)

"Mechanical Sympathy"

—-

    CERN-video: 
    10:00 CERN is no longer special, facebook/google/netflix/twitter 
     are bigger/similar, GitHub/sharing, today we share
    11:20: tools! (and capacity)
    16:15: incoming Linux, outgoing Puppet Openstack Kibana
    17:15 give us your budget and we'll give you a quota
    17:20 those who know Puppet and OpenStack
    
    13:35 some updates numbers
    https://www.youtube.com/watch?v=OOofolfR1rk

[Pets and
Cattle](https://cloudscaling.com/blog/cloud-computing/the-history-of-pets-vs-cattle)
(Creative commons)

[Reverse uptime and golden image
freshness](https://diogomonica.com/2017/08/31/two-metrics-that-matter-for-host-security/)

## About the Lab Exercises

![image](/home/erikhje/office/kurs/iac/01-introiac/tex/net-setup-fig.pdf)

Note: text in `UPPER_CASE` should be replaced by you when you type the
example commands.

## Review questions and problems

1.  What is a *security group* in OpenStack? What do you use it for?

2.  What are the two main principles of *Infrastructure as Code*?

3.  What is *automation fear*? How can it happen?

4.  An IT infrastructure should not only be robust, it should be
    *antifragile*. What does this mean?

5.  The textbook states “An infrastructure team should seek out and read
    through every whitepaper, article, conference talk, and blog post
    they can find about the platform they’re using”. Why? What is the
    motivation behind this statement?

6.  In pseudo code, write an ordered list (reflecting the sequence you
    would run) of `openstack` (or `nova` and `neutron` commands)
    (including all command line arguments) to set up your own router,
    network and Ubuntu server in SkyHiGh in such a way that you can
    reach it with ssh from other hosts in the NTNU-network (similar to
    what we have done in the lab).

## Lab tutorials

1.  You will be using NTNUs private OpenStack cloud
    [SkyHiGh](https://www.ntnu.no/wiki/display/skyhigh). OpenStack is an
    cloud solution composed of many sub projects. SkyHiGh is accessible
    as long as you are in the NTNU network (use VPN if you need access
    from outside).
    
    Each OpenStack project has its own client, but these individual
    clients are now deprecated in favor of a common `openstack` client
    to standardize and simplify the interface :
    
    > The following individual clients are deprecated in favor of a
    > common client. Instead of installing and learning all these
    > clients, we recommend installing and using the OpenStack client.
    
    You are expected to solve this exercise using the [Openstack
    Command-line
    Client](https://docs.openstack.org/python-openstackclient/latest/)
    
    (note: in some special cases you might have to still use to old
    commands if the `openstack` command does not support the operation)
    
    (btw for tab completion of subcommands do something like  
    `openstack complete >> ~/.bashrc`)
    
    You will be provided with all commands you need to execute most of
    the time, but make sure you understand what is happening (not just
    copy and paste without thinking). This is the foundation for scaling
    up operations later in the course.
    
    *Parts of commands that are written in UPPERCASE are meant to be
    replaced by you*.
    
    Tip: View your infrastructure in  
    <https://skyhigh.iik.ntnu.no/horizon/project/network_topology/>  
    as we go along.
    
      - To access to lab from outside networks, you need to be connected
        by VPN to the NTNU network, see [Install
        VPN](https://innsida.ntnu.no/wiki/-/wiki/English/Install+VPN)
    
      - For RDP on Windows, use built-in Remote Desktop Connection
        (`mstsc`), if you are using Windows 8 or newer, consider
        applying this fix (you probably have to apply only the first
        one) first:  
        <http://chall32.blogspot.no/2012/04/fixing-remote-desktop-annoyances.html>
        
        On Mac, get Microsoft Remote Desktop connection from the App
        store.
        
        On Linux, use something like:  
        `xfreerdp /u:Admin +clipboard /w:1366 /h:768 /v:SERVER_IP`
    
    <!-- end list -->
    
    1.  Decide on where you want your work environment to be: on your
        laptop or on a server (e.g. `login.stud.ntnu.no`). If you use
        your laptop, you need to install the
        [python-openstackclient](https://github.com/openstack/python-openstackclient).
        If you ssh to a server you are strongly advised to use tmux (or
        screen if you prefer) session. If you just want to get started
        fast, I recommend you ssh to `login.stud.ntnu.no`.
        
        If you use Windows, you can install OpenSSH with [Installation
        of OpenSSH For Windows Server 2019 and Windows
        10](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse).
    
    2.  Familiarize yourself with the most widely used OpenStack CLI
        commands:
        
            openstack -h | less
        
        Set environment variables using the OpenStack RC file:
        
        1.  Login to SkyHiGh GUI, “API Access”, “Download OpenStack RC
            file” (Identity API v3)
        
        2.  Copy this file to your work environment (e.g.  
            `scp PRIV_user-openrc.sh user@login.stud.ntnu.no:`)
        
        3.  Source the file: `. PRIV_user-openrc.sh`
        
        4.  Check that you now can run commands without problems, e.g.  
            `openstack keypair list`
        
        Alternatively, if you don’t want to have your cleartext password
        as a environment variable, see [Authenticating the CLI-client
        using
        tokens](https://www.ntnu.no/wiki/display/skyhigh/Authenticating+the+CLI-client+using+tokens).
    
    3.  Create a key pair (e.g. use your username as key name). Take
        good care of the private key\!
        
            openstack keypair create KEY_NAME > KEY_NAME.pem
    
    4.  Create a network.
        
            openstack network create net1
            # old deprecated way:
            neutron net-create net1
    
    5.  Create a subnet `192.168.1XY.0/24` where `XY` is your choice.
        
            openstack subnet create subnet1 --network net1 \
             --subnet-range 192.168.1XY.0/24
            # old deprecated way:
            neutron subnet-create net1 192.168.1XY.0/24 --name subnet1
    
    6.  Create a router.
        
            openstack router create router1
            # old deprecated way:
            neutron router-create router1
    
    7.  Set the router’s gateway to be in `ntnu-internal`.
        
            openstack router set router1 --external-gateway ntnu-internal
            # old deprecated way:
            neutron router-gateway-set router1 ntnu-internal
    
    8.  Add an interface on the router to your subnet.
        
            openstack router add subnet router1 subnet1
            # old deprecated way:
            neutron router-interface-add router1 subnet1
        
        You now have a network where you can boot your servers. If you
        need to delete this setup, deleting it can sometimes be tricky
        due to dependencies, but something like this should work (it
        also worth noting that sometimes it is easier to delete stuff
        using the GUI (Horizon) than the command-line):
        
            # deleting what you just created:
            openstack router remove subnet router1 subnet1
            openstack router delete router1
            openstack subnet delete subnet1
            openstack network delete net1
    
    9.  Boot the most recent LTS Ubuntu release available in glance (the
        image store, `openstack image list`) in your network (Remember
        to use your key, it will be injected into the instance by
        cloud-init, and you will use it to log in later).
        
            openstack server create --image NAME_OR_ID --flavor NAME_OR_ID \
             --nic net-id=UUID --key-name KEY_NAME INSTANCE_NAME
            # old deprecated way:
            nova boot --flavor NAME_OR_ID --image NAME_OR_ID --nic net-id=UUID \
             --key-name KEY_NAME INSTANCE_NAME
    
    10. Boot the most recent Windows server available in glance in your
        network. Remember to use your key, it will be used by
        cloudbase-init to injected a random password for the user
        `Admin` which you can retrieve with:
        
            nova get-password INSTANCE_NAME KEY_NAME.pem
    
    11. Create floating ips in the `ntnu-internal` and associate them
        with the Ubuntu and Windows instance.
        
            openstack floating ip create ntnu-internal
            openstack server add floating ip INSTANCE_NAME_OR_ID FLOATING_IP_ADDRESS
            # old deprecated way:
            nova floating-ip-create ntnu-internal
            nova floating-ip-associate INSTANCE_NAME_OR_ID FLOATING_IP_ADDRESS
    
    12. Add a security rule to the `default` security group to allow
        ping.
        
            openstack security group rule create --protocol icmp \
             --remote-ip 0.0.0.0/0 default
            # to list all the rules in this security group:
            openstack security group rule list default
            # old deprecated way:
            nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0
    
    13. Add security rules for http and https also to the `default`
        security group.
        
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 80 default
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 443 default
            # old deprecated way:
            nova secgroup-add-rule default tcp 80 80 0.0.0.0/0
            nova secgroup-add-rule default tcp 443 443 0.0.0.0/0
    
    14. Create a security group `linux` with a rule allowing ssh. Add
        this security group to the Ubuntu instance.
        
            openstack security group create linux
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 22 linux
            openstack server add security group INSTANCE_NAME linux
            # old deprecated way:
            nova secgroup-create linux "Rules for Linux"
            nova secgroup-add-rule linux tcp 22 22 0.0.0.0/0
            nova add-secgroup INSTANCE_NAME linux
    
    15. Create a security group `windows` with a rule allowing rdp. Add
        this security group to the Windows instance.
        
            openstack security group create windows
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 3389 windows
            openstack server add security group INSTANCE_NAME windows
            # old deprecated way:
            nova secgroup-create windows "Rules for Windows"
            nova secgroup-add-rule windows tcp 3389 3389 0.0.0.0/0
            nova add-secgroup INSTANCE_NAME windows
    
    16. Create two 1GB volumes with display names `linux_vol` and
        `windows_vol` and attach them to the Ubuntu and Windows
        instances, respectively.
        
            openstack volume create --size 1 linux_vol
            openstack volume create --size 1 windows_vol
            openstack server add volume INSTANCE_NAME_OR_ID VOLUME_NAME_OR_ID
            # old deprecated way:
            cinder create --display-name linux_vol 1
            cinder create --display-name windows_vol 1
            nova volume-attach INSTANCE_ID VOLUME_ID
    
    17. Log in to the Ubuntu instance, format, create filesystem and
        mount the new volume on `/myvol`.
        
            cfdisk /dev/vdb
            mkfs.ext4 /dev/vdb1
            mkdir /myvol
            mount /dev/vdb1 /myvol
    
    18. Log in to the Windows instance (username `Admin`), use `Disk
        management` to initialize, create volume, filesystem and mount
        the new volume on `D:\`.
        
            # Right click start button, Disk management, 
            # Initialize and bring online, Create volume
            # or in PowerShell (thanks Martin and Kristoffer)
            # Find disk number:
            Get-Disk
            # Initialize
            Initialize-Disk DISK_NUMBER
            # Partition and format:
            New-Partition -DiskNumber DISK_NUMBER -UseMaximumSize `
             -AssignDriveLetter | Format-Volume
    
    19. Disable SMBv1 on the Windows instance.
        
            Get-WindowsFeature FS-SMB1
            Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol
            #
            # some other windows stuff you might want to do:
            ##############################################################
            # on Windows 10 and newer, to get the ssh command you can do:
            # PowerShell -> Run as administrator
            Set-ExecutionPolicy RemoteSigned
            Install-PackageProvider Chocolatey
            # Restart PowerShell -> Run as administrator
            # Visit https://chocolatey.org/packages and check that most 
            # recent version of nano and openssh are marked as 
            # approved/trusted
            Install-Package nano, openssh
            # Exit PowerShell, start PowerShell as yourself (not adminstrator)
            C:\chocolatey\bin\nano $profile
            # enter the following line, then save the file
            $env:Path += ";C:\Chocolatey\bin;C:\Program files\OpenSSH-Win64"
            ##############################################################
    
    20. Install nginx on the Ubuntu instance, verify that it is up and
        running by browsing its floating ip.
    
    21. Delete everything you have created except your keypair and your
        security groups.

# Infrastructure Definition Tools

## Textbook chapter three

1.  CLI commands

2.  script CLI commands....scale, missing idempotency, create and delete

3.  declarative language, compose infrastructures as stacks with a
    *Configuration definition file* (generic term for Heat code in our
    case now, any kind of declarative code for defining infrastructure
    elements)

Provisioning: everything involved in making an element ready for use

Orchestration tools:

  - OpenStack: Heat (yaml)

  - Hashicorp: Terraform (HashiCorp Configuration Language (HCL))

  - Amazon: Cloudformation (yaml/json)

  - Azure: Azure Resource Manager (json)

  - Google Compute Engine: Cloud Deployment Manager (yaml)

  - ...

Passing information\! Service discovery and Configuration registry
(KeyValue-db/DNS):

  - Consul

  - Etcd

  - Zookeeper

  - Mesos-DNS

  - Minuteman

  - SkyDNS

  - SmartStack

  - Eureka

  - (PuppetDB, Ansible Tower, ChefServer, ...)

Configuration registry is a newer concept than CMDB (but overlaps)

## Textbook chapter nine

A stack is a collection of infrastructure elements defined as one unit

Parameterize and create reusable definition files

Avoid huge monolithic stack:  
\- separate into smaller stacks: scope of change\!

Run your tools from central server most times (known environment)  
(`login.stud.ntnu.no`)

## Review questions and problems

1.  How does a Heat resource retrieve/make use of a value passed as a
    parameter to the template? How does a Heat resource reference
    another Heat resource?

2.  Describe the main requirements for an Infrastructure Definition
    Tool.

3.  What is the difference and the similarity between a configuration
    registry and a CMDB?

4.  What is a “stack”? What properties/characteristics has a stack?

5.  What is a good rule-of-thumb (guiding principle) when splitting up
    your infrastructure into multiple stacks?

6.  After you have completed this chapters lab tutorial, modify the
    template in such a way that the two servers can be different (e.g.
    an Ubuntu and a Windows instance). Also allow for a list of security
    groups to be added as parameters to each server. Delete the Heat
    stack from the previous exercise and launch it again but this time
    with an Ubuntu and a Windows instance (with the linux and windows
    security groups, respectively).

## Lab tutorials

1.  Create a new network with two Ubuntu instances by using the Heat
    template `servers_in_new_neutron_net.yaml` from (remember to always
    examine code that you reuse, but this is from the repo of the
    OpenStack Heat developers so this is probably good code, and reusing
    good code is a best practice we should enforce)
    [openstack/heat-templates](https://github.com/openstack/heat-templates/blob/master/hot)
    
        openstack stack create -t servers_in_new_neutron_net.yaml \
         -e heat_demo_env.yaml heat_demo
    
    Your environment file `heat_demo_env.yaml` should look something
    like this (the environment file is for enforcing what should be a
    well known principle for you: *separating code and data*)
    
        parameters:
          key_name: KEY_NAME
          image: Ubuntu Server 18.04 LTS (Bionic Beaver) amd64
          flavor: m1.small
          public_net: ntnu-internal
          private_net_name: net1
          private_net_cidr: 192.168.1XY.0/24
          private_net_gateway: 192.168.1XY.1
          private_net_pool_start: 192.168.1XY.200
          private_net_pool_end: 192.168.1XY.250
    
    If you get an error immediately when trying to create a stack you
    probably have a syntax error (e.g. your Yaml file does not have
    correct indentation or similar). If syntax is OK, but the stack
    fails to create e.g. `CREATE_FAILED` message or similar, try
    something like  
    `openstack stack event list heat_demo --nested-depth 3`

# Server Config Tools: Declarative Programming

## Textbook chapter four

Server lifecycle

Small number of well-known tools

VMs vs containers

## CMS: declarative programming

CMS is an abbreviation for many things (content management system, etc),
in our context it means *Configuration Management System*.

start a heat stack similar to last week, log in and `sudo -s`

a manifest: hello world

puppet apply vs master-agent setup

puppet resource

demo wp w docker (do the other lab yourself)

Where are we heading?

    role (profiles combined into roles, 
    a host typically only has a single role)
    ---
    profiles (combined tasks)
    ---
    modules (simple dedicated tasks)
    ---
    puppet resources (user, group, file, package, ...)
    ---
    platformspecific 
    (Windows-user, Linux-user, 
    ACL-on Windows, ACL-on Linux, 
    msi/chocolatey/apt/yum, ...)

## Review questions and problems

1.  What is a *resource type* in Puppet? give three examples of resource
    types.

2.  What is the Puppet Resource Abstraction Layer (RAL)?

3.  You can get the ip (v4) address of a host with `facter ipaddress`,
    but how do you access this same information as a top-scope variable
    in a Puppet manifest?

4.  Given that all services your team is responsible for are
    containerized (run in containers), how does this simplify server
    management?

5.  Write a Puppet manifest that ensures your home directory
    (`/home/ubuntu`) is only accessible by you (permission `0750`).

6.  Write a Puppet manifest that ensures `/home/ubuntu/public_html` is a
    symbolic link to `/var/www/ubuntu`. Is `/var/www/ubuntu` created if
    it does not exist?

7.  Write a Puppet manifest that ensures that the packages `vim`,
    `nano`, `jed` and `jove` are installed. Use an array to store the
    list of packages so you only need to declare the `package` resource
    type once.

8.  Write a Puppet manifest that ensures the user `data` exists with
    encrypted password `$6$38Tdw...` and belonging to the `sudo` group.
    Also make sure the group `data` exists before ensuring the user
    `data` exists.

9.  Write a Puppet manifest which does the following *in sequence*:
    
    1.  ensure the ssh package is in its latest version.
    
    2.  edit the config file `sshd_config`, use the following resource
        declaration for this:
        
            augeas { 'sshd_config':
              context => '/files/etc/ssh/sshd_config',
              changes => 'set X11Forwarding yes',
            }
    
    3.  make sure that the ssh service is running and restarted if its
        configuration has been changed.

10. Write a Puppet manifest that makes sure the software package
    `mybackup` is installed in its latest version, and creates a cron
    entry to run the command  
    `mybackup /home/data` as root every half hour. The manifest must
    include a notify relationship between the package and cron resource
    declarations.

## Lab tutorials

1.  Launch the Puppet learning-VM `Puppet Learning VM 2019.0.2 6.10` in
    SkyHiGh (you will find it in Glance, choose a flavor with 2 vcpus)
    and do everything you are asked to do in the chapters
    
      - Welcome
    
      - Hello Bolt
    
      - Hello Puppet
    
      - Agent Run
    
      - Manifests and Classes
    
      - Package File Service
    
      - Variables and Templates
    
      - Class Parameters
    
      - Facts
    
      - Conditional Statements
    
      - The Forge
    
      - Roles and Profiles
    
    of the Quest Guide.
    
    To access the Quest Guide and get started, enter the learning-VM’s
    IP address in your browser’s address bar (be sure to use
    `http://<IPADDRESS>` for the Quest Guide, as `https://<IPADDRESS>`
    will take you to the PE console) or see
    [Summary](https://github.com/puppetlabs/puppet-quest-guide/blob/master/summary.md)
    
    You can log into the learning-VM with ssh as root, but you need to
    access the console in Horizon to see the password on the splash page
    (make a note of this password).
    
    If you get error logging in due to terminal settings, try to do
    
        export TERM=xterm

# Server Config Tools: Modules, Reuse, Testing

## CMS: Modules

last weeks model of role/profile

blackboard schedule, what you need to know about testing (impacts your
project)

notes of first video

example testing with solution to gossinbackup module

lab exercise - quickly

puppet forge (importance of approved modules) - your chance to prove
yourself, code/contribute\! many bad modules\!

problems - spend time on gossinbackup: README.md\!, you will learn:

  - module structure with best practice layout, "api" to your module

  - puppet language in practice with dependencies and order

  - inherits, variables and namespace, array, function, four different
    resource types

  - reusing other code, inifile and vcsrepo (choosing the proper modules
    to reuse, STUDY THEIR READMEs\!)

demo result on test VM:

  - create backup dir

  - module install vcsrepo and inifile

  - unzip module in prod dir

  - apply manifest

  - examine cron job, demo run from /opt

`pdk new module gossinbackup`

Where we are heading, THIS is professional sysadm, sysadm is not
plugging cables into switches (that’s netadm :) :

“If your roles and profiles modules are separate from your control repo,
you end up having to push changes to, say, a class in the profiles
module, then updating the Puppetfile in the control repo, then trigger
an R10k run/sync. If things aren’t correct, you end up changing the
profile, pushing that change to the profile repo, and THEN having to
trigger an R10k run/sync (and if you don’t have SSH access to your
masters, you have to make a dummy commit to the control repo so it
triggers an R10k run OR doing a curl to some endpoint that will update
your Puppet master for you)”

## Review questions and problems

1.  Give a brief overview of how you can test Puppet modules (from the
    simplest to the most advanced testing).

2.  Install the module `puppetlabs-motd`. See the [README
    file](https://forge.puppet.com/puppetlabs/motd) for examples of how
    to use it. Write a Puppet manifest where you create the hash
    
        $motd_hash = {
          'fqdn'       => $facts['networking']['fqdn'],
          'os_family'  => $facts['os']['family'],
          'os_name'    => $facts['os']['name'],
          'os_release' => $facts['os']['release']['full'],
        }
    
    and when you declare the class `motd` let the parameter `content`
    be  
    `epp('motd/motd.epp', $motd_hash)`  
    (see [Creating templates using Embedded
    Puppet](https://puppet.com/docs/puppet/latest/lang_template_epp.html)
    for an explanation of the `epp()` function). Do a `puppet apply`,
    log out and back in to see the result. Btw, where is the file
    `motd/motd.epp` in the file system?

3.  Write a Puppet module `gossinbackup` which installs and configures
    the code from <https://github.com/githubgossin/gossin-backup>. The
    module should allow the following parameters to be configured:
    
    |                         |                                                            |
    | :---------------------- | :--------------------------------------------------------- |
    | `source_directory`      | (REQUIRED)                                                 |
    | `destination_directory` | (REQUIRED)                                                 |
    | `exclude`               | (default `/opt/gossin-backup/rsyncexcludes`)               |
    | `days`                  | (how many daily backups to keep, default `7`)              |
    | `months`                | (how many monthly backups to keep, default `12`)           |
    | `years`                 | (how many yearly backups to keep default `3`)              |
    | `max_file_size`         | (default `100M`)                                           |
    | `exec_hours`            | (which hours this will execute, default every hour: `*/1`) |
    | `exec_minutes`          | (minutes of hour this will exec, default `[ 15, 45 ]`)     |
    

    Read [Module
    fundamentals](https://puppet.com/docs/puppet/latest/modules_fundamentals.html),
    use the [Beginner’s Guide to
    Modules](https://puppet.com/docs/puppet/latest/bgtm.html) and study
    the puppetlabs-ntp module mentioned there. Your module should have
    the following classes:
    
      - `gossinbackup`
    
      - `gossinbackup::install`
    
      - `gossinbackup::config`
    
    Your module should depend on the `puppetlabs-stdlib` so it can
    define some of the parameters as `Stdlib::Absolutepath` data type.
    
    Your module should depend on the `puppetlabs-vcsrepo` module and use
    it for installing the script by cloning the repo on GitHub.
    
    Your module should depend on the `puppetlabs-inifile` module and use
    it for editing the configuration file (`gossin-backup.conf`).
    
    Simplest possible use of your module should be:
    
        class { 'gossinbackup':
            source_directory      => '/home',
            destination_directory => '/backups',
        }

## Lab tutorials

1.  Create a new stack with an Ubuntu instance, and log into it.

2.  Install the most recent version of Puppet on the server (NOTE:
    PUPPET COMMANDS SHOULD BE RUN AS ROOT (`sudo -s` or `sudo -i`) SINCE
    PUPPET IS DOING SYSTEM MAINTENANCE):
    
        # from:
        # https://docs.puppet.com/puppet/latest/reference/install_linux.html
        #
        
        wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
        sudo dpkg -i puppet6-release-bionic.deb
        sudo apt-get update
        sudo apt-get install puppet-agent
        # add Puppet binaries to PATH:
        echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
        # source .bashrc to apply:
        . ~/.bashrc

3.  Install the `puppetlabs-ntp` and `saz-timezone` modules and write a
    manifest `time.pp` which declares ntp and configures timezone to be
    `Europe/Oslo`. Apply the manifest with `puppet apply` (apply it
    repeatedly until it has converged, meaning it only generates the
    minimum output every time you apply it).
    
        puppet module install puppetlabs-ntp
        puppet module install saz-timezone
        
        # time.pp:
        
        node default {
          include ntp
          class { 'timezone':
            timezone => 'Europe/Oslo',
          }
        }

4.  Install the modules `puppetlabs-apache` and `hunner-wordpress` and
    write a manifest which installs wordpress (according to the README
    at <https://github.com/hunner/puppet-wordpress>)
    
    (when you have completed this exercise, let’s discuss WordPress
    security and how it relates to your module...)
    
        puppet module install puppetlabs-apache
        puppet module install hunner-wordpress
        puppet module list --tree
        
        # wordpress.pp:
        
        node default {
        
          $mysql_password        = 'put_in_hiera'
          $wordpress_password    = 'put_also_in_hiera'
        
          # PHP-enabled web server
          class { 'apache':
            default_vhost => false,
            mpm_module    => 'prefork',
          }
          class { 'apache::mod::php':
              php_version => '7.2',
          }
        
          # Virtual Host for Wordpress
          apache::vhost { $::fqdn:
            port           => '80',
            docroot        => '/opt/wordpress',
            manage_docroot => false,
          }
        
          # MySQL server
          class { 'mysql::server':
            root_password => $mysql_password,
          }
          class { 'mysql::bindings':  # if added late, need to restart apache service
            php_enable => true,
            php_package_name => 'php-mysql',
          }
        
          # Wordpress
          user { 'wordpress':
            ensure => 'present'
          }
          class { 'wordpress':
            version     => '4.9.7',   # if changed, delete index.php, rerun puppet
                                      # due to creates => "${install_dir}/index.php"
                                      # in manifests/instance/app.pp
            wp_owner    => 'wordpress',
            wp_group    => 'wordpress',
            db_user     => 'wordpress',
            db_password => $wordpress_password,
            require     => [ Class['apache'], Class['mysql::server'], User['wordpress'] ],
          }
        }

5.  ssh to another Linux server or delete and recreate the one you just
    used, and install puppet. Now lets install wordpress using a micro
    architecture with Docker, but managed by Puppet.
    
        puppet module install puppetlabs-docker
        
        node default {
        
          $mysql_password        = 'put_in_hiera'
          $wordpress_password    = 'put_also_in_hiera'
        
          class { 'docker':
            dns => '129.241.0.201',
          }
        
          class {'docker::compose':
            ensure  => present,
            version => '1.14.0',
          }
        
          file { '/tmp/docker-compose.yml':
            ensure  => present,
            content => "
        version: '3.3'
        
        services:
           db:
             image: mysql:5.7
             volumes:
               - db_data:/var/lib/mysql
             restart: always
             environment:
               MYSQL_ROOT_PASSWORD: $mysql_password
               MYSQL_DATABASE: wordpress
               MYSQL_USER: wordpress
               MYSQL_PASSWORD: $wordpress_password
        
           wordpress:
             depends_on:
               - db
             image: wordpress:latest
             ports:
               - \"8000:80\"
             restart: always
             environment:
               WORDPRESS_DB_HOST: db:3306
               WORDPRESS_DB_USER: wordpress
               WORDPRESS_DB_PASSWORD: $wordpress_password
        volumes:
            db_data: {}
        "
          }
        
          docker_compose { '/tmp/docker-compose.yml':
            ensure  => present,
            require  => [ File['/tmp/docker-compose.yml'], 
                          Class['docker::compose'], 
                          Class['docker'], ],
          }
        
        }
        # in case of error "Could not evaluate: undefined method" cd /tmp and 
        # docker-compose up just for demo purpose
        # btw, this wordpress will be at port 8000, does your security group allow
        # traffic to port 8000?

# Infrastructure Services

[An example of a simple module that Eigil and Lars Erik had to write in
their jobs](https://github.com/ntnusky/puppet-hpacucli)

## Textbook chapter four

### Server Lifecycle

##### Server Lifecycle

    Vendor image
         |
         |          PACKAGE
         V
    Server template
         |
         |          CREATE or REPLACE
         V
    Running server  UPDATE
         |
         |
         V
    Deleted server

Deploying av server should be *100% automatic*, not 99% (NO HUMAN
INVOLVEMENT, not even a single ’Ok’ click)

### Pull vs Push

##### Pull vs Push

  - Clients pulling at random time interval are good for stability (give
    clients autonomy) and security, scales better. Some times ad hoc
    push is necessary, and in smaller environments push might be better
    overall. (demo ssh push)

  - Scripting languages and tools: choose good tools AND GET TO KNOW
    THEM\!

### Models

##### Server Change Management Models

  - Ad hoc change managment  
    (what we did before config management)

  - Config sync  
    (e.g. puppet agent every 30 min +/- random offset)

  - Immutable infrastructure  
    (replace instead of update)

  - Containerized services  
    (simpler hosts with lower rate of change)

## Textbook chapter five

### Core Principles

##### Core Principles

Remember core principles: *Repeatability and Consistency*

  - Repeatability  
    delete-and-replace, delete-and-replace, ...

  - Consistency  
    whenever you delete-and-replace or create multiple instances of a
    service, it should be configured in the same way (lead to the same
    state).

("Face your fear" don’t be afraid of change)

### Infrastructure Services

##### Infrastructure services

  - Time sync (NTP)

  - DHCP

  - Directory services (DNS/LDAP/HTTP(S)) and configuration registry

  - Configuration Management System

  - Databases

  - IDs and Authentication/Authorization

  - Monitoring

  - Logging

  - Message queues

  - ...

Infrastructures: show old figure from Traugott\&Huddleston , search
"throw"

### Externalized Config

##### Externalized Configurations

Choose products with externalized configuration and that ’understands’
dynamic environments (if a server disappears the monitoring system
should not scream "Critical" every time ...)

## Review questions and problems

1.  Describe what is meant by “an idempotent operation”.

2.  What is service discovery? Briefly discuss some approaches to
    service discovery.

3.  What is maybe the most challenging part/key issue of centralized
    logging (ELK/Splunk/...)?

## Lab tutorials

1.  *Launch infrastructure.* (Remember that you can always log in to
    <https://skyhigh.iik.ntnu.no> and view your Network Topology to get
    an overview of your infrastructure). It should look something like
    figure [5.2](#fig:rest).
    
    ![Your network (graph view and topology view) after launching more
    servers.<span label="fig:rest"></span>](/home/erikhje/office/kurs/iac/00-LAB/../00-ILLUSTRATIONS/rest_graph.png)
    ![Your network (graph view and topology view) after launching more
    servers.<span label="fig:rest"></span>](/home/erikhje/office/kurs/iac/00-LAB/../00-ILLUSTRATIONS/rest_topology.png)
    
    Make sure you have a keypair and the linux and windows security
    groups. Download or clone the git repo at  
    <https://github.com/githubgossin/IaC-heat>  
    and create the stack.
    
        # environment file contents:
        parameters:
          key_name: KEY_NAME
        
        # create the stack:
        openstack stack create -e iac_top_env.yaml -t \
         iac_top.yaml iac
    
    Note: WAIT a few minutes before retrieving the password for the
    Windows instance (`nova get-password win KEY_NAME.pem` or retrieve
    the password from web-gui: instances - ’Retrieve Instance Password’)
    and logging into the Windows server (Note: Server 2019 is not
    vulnerable to
    [Bluekeep](https://twitter.com/GossiTheDog/status/1128431661266415616),
    but in your future job you might be responsible for legacy servers
    that are vulnerable).

2.  *Install puppetserver* on `manager`:
    
        wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
        sudo dpkg -i puppet6-release-bionic.deb
        sudo apt-get update
        sudo apt-get install puppetserver
        echo 'PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
        . ~/.bashrc
        sudo -i
        echo "$(facter networking.ip) $(hostname).node.consul $(hostname)" >> /etc/hosts
        echo "$(facter networking.ip) puppet" >> /etc/hosts
        puppetserver ca setup
        puppet resource service puppetserver ensure=running enable=true
        #
        # ALSO install the agent on the server
        /opt/puppetlabs/bin/puppet config set server manager.node.consul --section main
        /opt/puppetlabs/bin/puppet config set runinterval 300 --section main
        /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
    
    Puppet agent should be installed on all the servers as part of the
    install sequence (cloud-init) and set to contact the puppetserver
    once randomly in every five minute period. Check to see which
    servers have done this, and wait until all of them appear in the
    list before signing all the keys.
    
        puppetserver ca list
        # all hosts should show up within 5 minutes
        # you can sign all keys with
        puppetserver ca sign --all
    
    Log in to one of the Linux servers and check that communication with
    the puppetserver is ok, should look something like this:
    
        root@mon:~# puppet agent -t
        Info: Using configured environment 'production'
        Info: Retrieving pluginfacts
        Info: Retrieving plugin
        Info: Retrieving locales
        Info: Caching catalog for mon.node.consul
        Info: Applying configuration version '1567617435'
        Notice: Applied catalog in 0.02 seconds
        root@mon:~#
    
    Do the same on the Windows server
    
        PS C:\windows\system32> puppet agent -t
        Info: Using configured environment 'production'
        Info: Retrieving pluginfacts
        Info: Retrieving plugin
        Info: Retrieving locales
        Info: Caching catalog for win.node.consul
        Info: Applying configuration version '1567617985'
        Notice: Applied catalog in 0.04 seconds
        PS C:\windows\system32>
    
    `puppet config print` is nice to know to inspect puppet’s
    configuration, try the following
    
        # where should modules be installed on the server?
        puppet config print modulepath
        # where is the manifest that assigns 
        # a configuration to a server?
        puppet config print manifest
        # how often does puppet agent run?
        puppet config print runinterval
    
    Reports from the puppet agents (do `ls -ltr` in one of the
    directories in here and look for the largest files, those contain
    reports of when puppet actually did something, the other files are
    just confirming that state is correct, remember idempotency)  
    `ls -R /opt/puppetlabs/server/data/puppetserver/reports/`  
    (You can get a fancy interface to reports if you install
    [PuppetDB](https://puppet.com/docs/puppetdb/latest) and use
    [Puppetboard](https://github.com/voxpupuli/puppetboard) or [Puppet
    Explorer](https://github.com/dalen/puppetexplorer))

3.  *Install base configuration on puppetserver and assign to the other
    servers.*
    
        # on manager
        puppet module install puppetlabs-ntp
        puppet module install saz-timezone
        puppet module install puppetlabs-dsc
        puppet module install puppetlabs-chocolatey
        apt install pdk # takes a few minutes
        pdk new module profile
        cd profile
        pdk new class profile::base_linux
        # fill manifests/base_linux.pp with content (see below)
        pdk new class profile::base_windows
        # fill manifests/base_windows.pp with content (see below)
        cp -r ../profile /etc/puppetlabs/code/environments/production/modules/
    
    Create a keypair to enable key-based ssh login to all servers from
    manager with `ssh-keygen` (you will now have a public key in
    `/root/.ssh/id_rsa.pub` which you will need later).
    
    Put all your site-specific data in Hiera (at least the ssh public
    key and the list of software to be installed), the file  
    `/etc/puppetlabs/code/environments/production/data/common.yaml`  
    should look like
    
        ---
        base_windows::win_sw_pkg:
          - 'notepadplusplus'
          - 'git'
        base_linux::linux_sw_pkg:
          - 'htop'
          - 'sysstat'
          - 'vim'
        base_linux::root_ssh_key: 'YOUR_PUBLIC_SSH_KEY'
    
    In your profile module `manifests/base_linux.pp` should look like
    
        #
        # profile::base_linux
        #
        
        class profile::base_linux {
        
          $root_ssh_key = lookup('base_linux::root_ssh_key')
          $linux_sw_pkg = lookup('base_linux::linux_sw_pkg')
        
        # careful when configuring ntp to avoid misuse (opening for DDOS)
          class { 'ntp':
            servers  => [ 'ntp.ntnu.no' ],
            restrict => [
              'default kod nomodify notrap nopeer noquery',
              '-6 default kod nomodify notrap nopeer noquery',
            ],
          }
          class { 'timezone':
            timezone => 'Europe/Oslo',
          }
        
          package { $linux_sw_pkg:
            ensure => latest,
          }
        
        # root@manager should be able to ssh without password to all
        
          file { '/root/.ssh':
            owner  => 'root',
            group  => 'root',
            mode   => '0700',
            ensure => 'directory',
          }
          ssh_authorized_key { 'root@manager':
            user    => 'root',
            type    => 'ssh-rsa',
            key     => $root_ssh_key,
            require => File['/root/.ssh'],
          }
        
        }
    
    In your profile module `manifests/base_windows.pp` should look like
    
        #  
        # profile::base_windows
        #
        
        class profile::base_windows {
        
          $win_sw_pkg = lookup('base_windows::win_sw_pkg')
        
        # set default package provider on windows to chocolatey,
        # please read https://chocolatey.org/security
        # in the future maybe use DSC instead when this is ready:
        # https://github.com/PowerShell/PackageManagementProviderResource
        
          include chocolatey
        
        # set chocolatey as default package provider on Windows
        
          case $::operatingsystem {
            'windows':
              { Package { provider => chocolatey, } }
            default:
              { Package { provider => windows, } }
          }
        
          package { $win_sw_pkg:
            ensure => 'latest',
          }
        
        # a specific version of vim I know is trusted
          package { 'vim':
            ensure => '8.0.604',
          }
        
        # use PowerShell DSC to set timezone
          dsc_xtimezone { 'Oslo':
            dsc_timezone         => 'W. Europe Standard Time',
            dsc_issingleinstance => 'yes',
          }
        
        # use PowerShell DSC protect against wannacry :)
          dsc_windowsfeature {'FS-SMB1': 
            dsc_ensure => 'absent', 
            dsc_name   => 'FS-SMB1', 
          }
        
        }
    
    Finally to assign the configuration do  
    `vi $(puppet config print manifest)/site.pp`  
    and make the file look like
    
        node default {
          notify { "Oops Default! I'm ${facts['hostname']}": }
        }
        node 'win.node.consul' {
          include ::profile::base_windows
        }
        node /(dir|mon|lin)\d?.node.consul/ {
          include ::profile::base_linux
        }
    
    Either log in to hosts and run `puppet agent -t` or just wait a few
    minutes and view the latest report to see if something happened
    (also check that the software was actually installed on windows and
    the linux servers).

4.  *Configuration Registry, Service Discovery and DNS*. In addition to
    time synchronization (and DHCP delivered from OpenStack), directory
    services for service discovery and DNS are core infrastructure
    services which should be in place before we do additional service
    deployments.
    
    Install the needed modules
    
        puppet module install --ignore-dependencies KyleAnderson-consul --version 5.1.0 
        puppet module install --ignore-dependencies camptocamp-systemd
        puppet module install --ignore-dependencies puppet-archive
        puppet module install --ignore-dependencies puppetlabs-inifile
        puppet module install puppetlabs-translate
        puppet module install --ignore-dependencies puppetlabs-concat
        puppet module install --ignore-dependencies zehweh-netplan
        /opt/puppetlabs/puppet/bin/gem install lookup_http
        puppetserver gem install lookup_http
        systemctl reload puppetserver.service
        puppet module install crayfishx/hiera_http
        cd /etc/puppetlabs/code/environments/production/modules
        git clone https://github.com/ppouliot/puppet-dns.git
        mv puppet-dns dns
    
    Update the `hierarchy`-part of  
    `/etc/puppetlabs/code/environments/production/hiera.yaml`  
    to (Note: this is a “hard coded hack” which we should generalize by
    not specifying `dir.node.consul` like we do here, maybe you can fix
    this in your project?)
    
        hierarchy:
          - name: "Per-node data (yaml version)"
            path: "nodes/%{::trusted.certname}.yaml"
          - name: "Other YAML hierarchy levels"
            paths:
              - "common.yaml"
          - name: "Consul"
            lookup_key: hiera_http
            uris:
              - "http://localhost:8500/v1/catalog/node/dir"
            options:
              dig_key: Node.__KEY__
              output: json
              failure: graceful
    
    Add more puppet code to the profile module
    
        cd profile
        pdk new class profile::consul::server
        # fill manifests/consul/server.pp with content (see below)
        pdk new class profile::consul::client
        # fill manifests/consul/client.pp with content (see below)
        pdk new class profile::dns::server
        # fill manifests/dns/server.pp with content (see below)
        pdk new class profile::dns::client
        # fill manifests/dns/client.pp with content (see below)
        cp -r ../profile /etc/puppetlabs/code/environments/production/modules/
    
    `manifests/consul/server.pp` should look like
    
        class profile::consul::server {
        
          package { 'unzip':
            ensure => latest,
          }
        
          class { '::consul':
            version     => '1.6.0',
            config_hash => {
              'bootstrap_expect' => 3,
              'data_dir'         => '/opt/consul',
              'datacenter'       => 'NTNU',
              'log_level'        => 'INFO',
              'node_name'        => $facts['hostname'],
              'server'           => true,
              'retry_join'       => [ $::serverip ],
            },
            require     => Package['unzip'],
          }
        
        }
    
    `manifests/consul/client.pp` should look like
    
        class profile::consul::client {
        
          package { 'unzip':
            ensure => latest,
          }
        
          class { '::consul':
            version     => '1.6.0',
            config_hash => {
              'data_dir'         => '/opt/consul',
              'datacenter'       => 'NTNU',
              'log_level'        => 'INFO',
              'node_name'        => $facts['hostname'],
              'retry_join'       => [ $::serverip ],
            },
            require     => Package['unzip'],
          }
        
        }
    
    `manifests/dns/server.pp` should look like
    
        class profile::dns::server {
        
          include dns::server
        
          # Forwarders
          dns::server::options { '/etc/bind/named.conf.options':
            dnssec_enable     => false,
            dnssec_validation => no,
            forwarders        => [ '129.241.0.201' ],
          }
        
          dns::zone { 'consul':
            zone_type       => forward,
            forward_policy  => only,
            allow_forwarder => [ '127.0.0.1 port 8600' ],
          }
        
        }
    
    `manifests/dns/client.pp` should look like
    
        class profile::dns::client {
        
          $dir_ip = lookup( 'Address', undef, undef, '1.1.1.1' )
        
          case $facts['os']['name'] {
            'windows': { 
              dsc_dnsserveraddress { $dir_ip:
                dsc_address        => $dir_ip,
                dsc_interfacealias => $facts['networking']['primary'],
                dsc_addressfamily  => 'IPv4',
                dsc_validate       => true,
              }
              dsc_dnsclientglobalsetting { 'domainname':
                dsc_issingleinstance => yes,
                dsc_suffixsearchlist => 'node.consul',
              }
            }
            /^(Debian|Ubuntu)$/: { 
              class { 'netplan':
                config_file   => '/etc/netplan/50-cloud-init.yaml',
                ethernets     => {
                  'ens3' => {
                    'dhcp4'       => true,
                    'nameservers' => {
                      'search'    => ['node.consul'],
                      'addresses' => [ "$dir_ip" ],
                    }
                  }
                },
                netplan_apply => true,
              }
            }
            default: { notify { 'Which OS? What?': } }
          }
        
        }
    
    Update `$(puppet config print manifest)/site.pp`  
    and make the file look like
    
        node default {
          notify { "Oops Default! I'm ${facts['hostname']}": }
        }
        node 'win.node.consul' {
          include ::profile::base_windows
          include ::profile::dns::client
        }
        node /lin\d?.node.consul/ {
          include ::profile::base_linux
          include ::profile::dns::client
          include ::profile::consul::client
        }
        node /(manager|mon).node.consul/ {
          include ::profile::base_linux
          include ::profile::dns::client
          include ::profile::consul::server
        }
        node 'dir.node.consul' {
          include ::profile::base_linux
          include ::profile::dns::client
          include ::profile::dns::server
          include ::profile::consul::server
        } 
    
    Wait a few minutes for magic to happen :)
    
    Check to see that you can do lookups
    
        # have everyone joined to consul service mesh?
        # windows not working yet...
        consul members
        
        # dns lookups ok?
        dig {dir,mon}.node.consul
        
        # run commands without password on other hosts
        # from manager (as root)
        for h in mon lin{0,1} dir; do ssh $h hostname ;done
        for h in mon lin{0,1} dir; do ssh $h hostname & done
        
        # puppet agents reports ok?
        for f in $(find /opt/puppetlabs/server/data/puppetserver/reports/* \
         -type f -cmin -5); do echo $f; cat $f | grep failure; done
        
        for f in $(find /opt/puppetlabs/server/data/puppetserver/reports/* \
         -type f -cmin -5); do echo $f; cat $f | grep "level: err"; done
        
        # apt install jq 
        # pip install yq
        for f in $(find /opt/puppetlabs/server/data/puppetserver/reports/* \
         -type f -cmin -5); do echo $f; cat $f | yq -y .metrics.events.values; done
    
    *Note: you can find all of Lars Eriks and Eigils [Puppet code for
    SkyHiGh on GitHub](https://github.com/ntnusky/profile).*

# Patterns for Provisioning and Updating Servers

## Textbook chapter six

### Provisioning servers

##### Three things on a server

  - Software

  - Configuration

  - Data

### Software origins

##### Where do software come from?

  - Internal repo

  - External repo (apt, yum, snap, pip, npm, cpan, gem, ....)

  - In the OS distro

  - Commercial enterprise software

### Accounts

##### Configuration: Accounts

  - When you create a server template, what to do with accounts?
    (root/admin especially)

  - Bootstrap a server from a template, cloudinit

## Textbook chapter seven

### Managing server templates

##### Server Templates

Using templates from others (stock templates) or role your own with e.g.
[Packer](https://packer.io/) ?

##### Server Templates

Small or large templates? (how much preinstalled?)

  - if very standardized environment and fast provisioning important,
    most stuff in the template

  - if great variation in environment, maybe have a small base template
    and config on provisioning

  - if servers have frequent changes, maybe faster to do with config
    management than with rebuilding a template

*Maybe use same tool (e.g. Puppet) for both configuring templates,
configuration on provisioning and for updates*

##### Server Templates

How do you update a template?

1.  change the template?

2.  copy a template to a new template and change it?

3.  other suggestions?

4.  *follow the same build process as always and build a new template
    with a new version number\!*

REPEATABILITY AND CONSISTENCY

## Textbook chapter eight

### Updating and changing servers

##### Updating and changing servers

  - Replace servers when the server template changes

  - Phoenix servers: give servers a max lifetime (*reverse uptime*)

  - Configuration management without a central/main server

  - Manage 100% of the "things" on a server

  - Immutable servers pattern

  - Keep configuration definitions minimal

## Workflow

##### Puppet Code Structure

  - See "Doing the refactor dance" video notes

  - [See figure at 03:06](https://youtu.be/RYMNmfM6UHw?t=186)

Problem: Control over module versions\!  
Solution: *Puppetfile*

See [What You Get From This
control-repo](https://github.com/puppetlabs/control-repo#what-you-get-from-this-control-repo)

##### r10k

![Workflow with
r10k.<span label="fig:r10k"></span>](/home/erikhje/office/kurs/iac/06-patterns/tex/../../00-ILLUSTRATIONS/r10k.pdf)

Puppet agents can ask the Puppet server for configuration from a
specific environment. Most of the time we use at least a testing and a
production environment. Figure [6.1](#fig:r10k) shows how code flow from
us into these environments on the Puppet server.

LAB: show what I have chosen as YOUR\_CHOSEN\_ID

\- log into remote git location, show control repo: Hiera, Puppetfile,
environment, site

\- demo workflow adding binclock

## Review questions and problems

1.  Explain the purpose and the content of the *control repo*.

2.  Explain how `git` is used in deploying puppet code in an
    infrastructure.

3.  Where do software on a server come from? List at least three
    possible sources and describe them briefly.

4.  Discuss benefits and drawbacks of having small and simple templates
    vs having large template with much software preinstalled.

5.  Discuss push vs pull for configuration synchronization.

6.  One way to completely avoid configuration drift is to try to manage
    100% of the “things” on a server. Discuss why this is or is not a
    good idea.

## Lab tutorials

1.  We are now going to use the workflow with local code being pushed to
    a git server, and r10k pulling this code and creating Puppet
    environments. Make an account on a remote git server (Bitbucket,
    GitHub or GitLab) if you don’t have one already that you would like
    to use.
    
    Create a new public key identity on `login.stud.ntnu.no` or your
    laptop (or wherever you prefer coding) AND a new public key identity
    on `manager` and upload them to your remote git-account. Choose to
    not have a password on your private key so we can automate this
    fully.
    
    Create ssh keypairs (I recommend you do not set a password on the
    private key in our case since it makes automation a bit easier) on
    `login.stud.ntnu.no` or your laptop (or whereever you want your
    development environment to be) and on `manager` and upload the
    public keys to your git account
    
      - [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh)
    
      - [Adding a new SSH key to your GitHub
        account](https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account)
    
      - [BitBucket: SSH
        keys](https://confluence.atlassian.com/bitbucket/ssh-keys-935365775.html)
    
    <!-- end list -->
    
        # create a ssh key as a new identity (with blank password or use ssh agent)
        ssh-keygen -f ~/.ssh/YOUR_CHOSEN_ID -C "YOUR_CHOSEN_ID" 
        
        # edit .ssh/config to contain (replace gitlab.com if you use 
        # BitBucket or GitHub). Note that 'StrictHostKeyChecking no'
        # is only needed by r10k so it bypasses ssh' initial yes/no
        Host YOUR_CHOSEN_ID
         HostName gitlab.com
         Preferredauthentications publickey
         StrictHostKeyChecking no
         IdentityFile ~/.ssh/YOUR_CHOSEN_ID
        
        cat .ssh/YOUR_CHOSEN_ID.pub 
        # and paste it into remote git account according to instructions

2.  Log into remote git and create an empty git repo `control-repo`

3.  Clone Puppetlabs’ template for a control repo to login.stud.ntnu.no
    (or your laptop), remove its origin and add the repo you created in
    Bitbucket as the new origin, push the production branch.
    
        git clone https://github.com/puppetlabs/control-repo.git
        git remote rm origin
        git remote add origin git@YOUR_CHOSEN_ID:USERNAME/control-repo.git
        git push origin production

4.  Copy your existing `site.pp` (from manager) into the control repo,
    commit and push.

5.  Copy your existing `hiera.yaml` (from manager) into the control
    repo, commit and push.

6.  Copy your existing `common.yaml` (from manager) into the `data`
    directory of the control repo, commit and push.

7.  Copy all manifests in your current profile module into
    `site-modules/profile/manifests` directory of your control repo,
    commit and push.

8.  Leave the `environment.conf` file as is in the control repo. This
    file will let Puppet know that the role and profile modules are in a
    different location (the `site-modules` directory) than the other
    modules.

9.  Modify the file `Puppetfile` in the control repo with the following
    contents (then commit and push of course):
    
        forge 'https://forge.puppet.com'
        
        # From previous lab
        mod 'KyleAnderson-consul',    '5.1.0'
        mod 'camptocamp-systemd',     '2.6.0'
        mod 'crayfishx-hiera_http',   '3.4.0'
        mod 'puppet-archive',         '3.2.1'
        mod 'puppetlabs-chocolatey',  '4.1.0'
        mod 'puppetlabs-concat',      '6.1.0'
        mod 'puppetlabs-dsc',         '1.9.3'
        mod 'puppetlabs-inifile',     '3.1.0'
        mod 'puppetlabs-ntp',         '8.0.0'
        mod 'puppetlabs-powershell',  '2.3.0'
        mod 'puppetlabs-reboot',      '2.2.0'
        mod 'puppetlabs-registry',    '2.1.0'
        mod 'puppetlabs-stdlib',      '6.0.0'
        mod 'puppetlabs-translate',   '2.0.0'
        mod 'saz-timezone',           '3.4.0'
        mod 'stm-debconf',            '2.3.0'
        mod 'zehweh-netplan',         '0.1.9'
        
        mod 'dns',
          git:    'https://github.com/ppouliot/puppet-dns',
          commit: 'ec588ed90bf64f97403170c7261944f86ccc16a3'
        
        # r10k
        mod 'puppet-r10k',            '7.0.0'
        mod 'puppetlabs-ruby',        '1.0.1'
        mod 'puppetlabs-vcsrepo',     '2.4.0'
        mod 'puppetlabs-git',         '0.5.0'

10. In our case, we just want two environments (I’m sure you see during
    this course how we could extend this with environments for
    development, or feature-based environments), `production` and
    `testing`. In the control repo, create a testing branch.
    
        git checkout -b testing
        git push origin testing

11. Install `r10k` on `manager`:
    
        puppet module install --ignore-dependencies puppet-r10k
        puppet module install --ignore-dependencies puppetlabs-ruby
        puppet module install --ignore-dependencies puppetlabs-vcsrepo
        puppet module install --ignore-dependencies puppetlabs-git
        
        vi /var/tmp/r10k.pp
        # fill this file with the following:
        class { 'r10k':
          sources => {
            'puppet' => {
              'remote'  => 'git@YOUR_CHOSEN_ID:USERNAME/control-repo.git',
              'basedir' => "/etc/puppetlabs/code/environments",
              'prefix'  => false,
            },
          },
        }
        
        # install r10k by applying:
        puppet apply /var/tmp/r10k.pp
    
    NOTE\! when you run r10k, it will delete and replace everything in  
    `/etc/puppetlabs/code/environments/`. Meaning: anything that is not
    managed by r10k in this directory will be deleted.

12. Deploy with `r10k`:
    
        # to deploy all environments:
        #
        r10k deploy environment -pv
        #
        # to deploy just testing:
        #
        r10k deploy environment testing -pv
        #
        # if you have problems check that the keys and config file are really in 
        # /root/.ssh and NOT in /home/ubuntu/.ssh (this might happen if you do 
        # 'sudo -s' and not 'sudo -i'   :)
        #
        # check that everything still runs fine (meaning, run puppet agent -t on
        # hosts and check that you do not get any errors), it you get hiera lookup 
        # problems, debug with a manual lookup:
        puppet lookup base_linux::root_ssh_key 
        # can add e.g. environment=production to this command
        #
        # Note: this seem to require that the entire catalog validates, which
        # really should not be necessary to just test a lookup

13. All hosts are by default in the `production` environment. Log into
    `lin0` and place it in the `testing` environment with  
    `puppet config set environment testing --section agent`  
    (See what happened: `cat /etc/puppetlabs/puppet/puppet.conf`) Hosts
    can also be [assigned to environment on the
    puppetserver](https://serverfault.com/a/846662) (the puppetserver
    can override the agent).

14. Add the package `binclock` to `profile::base_linux` in the testing
    branch of the profile repo, and deploy the testing environment to
    see that it only gets installed on `lin0`.

15. You can now add whatever Puppet-code you want to create new
    Profile-classes and then create Role-classes which contain a set of
    Profile classes. *A host should be assigned only one role class and
    no other classes at all.* Move all the profiles you are currently
    assigning to `dir` (in `site.pp`) into a role
    `role::directory_server` and change the corresponing node assignment
    in `site.pp` to
    
        node 'dir.node.consul' {
          include ::role::directory_server
        } 
    
    Commit, merge into production, push, pull with r10k, and see that
    puppet agent on `dir` is still happy :)

# Continuous Integration and Continuous Deployment (CI/CD)

## Textbook chapter 10

##### Repetition from Software Engineering

  - Version control/git w branches and branch strategies

  - CI - CDelivery - CDeployment

  - The practice of TDD and small changes

## Textbook chapter 11

##### Testing Infrastructure Changes

  - Testing: *catch errors early, faster dev*

  - Building tests organically

  - Test pyramid: *Low-, Medium- and High-level tests*

  - Test-infrastructure (Litmus, Beaker, r10k, test kitchen, ...)

  - Testing Operational quality / Non-functional requirements:
    
      - performance, availability and security

  - Isolate components for testing: mocks, fakes and stubs

  - *Those who code should also write the tests* (not separate test
    writers, but support in terms of "quality analyst")

## Textbook chapter 12

##### Change Management Pipeline

  - A pipeline for *continues small changes* instead of big batches

  - Pyramid pipeline tests in sequence (or in parallell)

  - Testing environments *do not have to be identical to production*

  - Run automated (“cheapest”) before manual tests

  - The build stage completes basic testing, publishes a "configuration
    artifact"

  - Practices
    
      - Every change you commit might end in production
    
      - Everyone should stop committing if a stage fails

## Textbook chapter 13

##### Workflow

  - Automate everything, *work in your infrastructure indirectly*
    instead of directly

  - Manually first to learn a task, then automate

  - Dont just build tools, automate to remove the human

  - Local virtualization for testing (Vagrant)

  - "Commit every hour", remember others commit also

  - Multiple git repos or one control repo?

  - Careful with branching... keep close to trunk (Master/Production)

  - *The best process for emergency fixes is the normal process*

  - Governance: "simple" when everything is logged (commits)

## Our workflow

##### Where to test?

![Relate to test
pyramid.<span label="fig:cicd"></span>](/home/erikhje/office/kurs/iac/07-cicd/tex/../../00-ILLUSTRATIONS/cicd.pdf)

Where is the main pipeline and what to test at the start of the pyramid
in figure [7.1](#fig:cicd)?

## Review questions and problems

1.  What is the best way to build a test suite for your existing
    infrastructure codebase (which currently lacks tests)?
    
    1.  Write tests for each new change that comes up.
    
    2.  Do not write any new code before you have written tests for all
        your existing code.
    
    3.  Only write tests when you find bugs.
    
    4.  Write tests for all your new code, and add tests for your old
        code when you have time.

2.  What does it mean to structure your test suite as a *test pyramid*?
    
    1.  A few low-level tests (unit tests, validation), more
        medium-level tests, and a lot of high-level tests.
    
    2.  All tests should be included at all levels, but in increasingly
        realistic environment.
    
    3.  All developers should write low-level (unit tests, validation)
        tests, while just a few dedicated testers should focus on high
        level tests.
    
    4.  A few high-level tests, more medium-level tests, and a lot of
        low-level tests (unit tests, validation).

3.  How can you isolate components for testing?
    
    1.  Simulate your environment related to all components that need
        testing.
    
    2.  You dont have to isolate components since you run an entire test
        pyramid.
    
    3.  Keep all class declarations in separate files.
    
    4.  Refactor components so they can be isolated and use test doubles
        (mocks, fakes, stubs).

4.  In the *immutable server model*, what is the configuration artifact?
    (artifact means “an object/product/thing resulting from a process”)
    
    1.  The Puppet catalog.
    
    2.  The server template image (or the container image).
    
    3.  A software package such as deb or rpm.
    
    4.  A version controlled file collection.

5.  Why should automated tests be done before doing manual tests?

## Lab tutorials

1.  Delete previous labs (remember to copy any data you want to keep).
    Launch the automated version of the previous labs
    [IaC-Heat-A](https://gitlab.com/erikhje/iac-heat-a). It takes about
    10 minutes, much of time is installing modules where you can watch
    progress with something like  
    `watch -n 5 ls
    /etc/puppetlabs/code/environments/production/modules/`

2.  *Testing:* Remember that
    
      - *when working alone on a small project, coding in small
        increments that build on each other, without any major
        refactoring, you will probably not feel the value of testing*
    
      - testing enables collaboration\! when others contribute code or
        you start refactoring your code you will truly see the value of
        testing
    
      - your tests should not be mirrors of puppet resource
        declarations: don’t duplicate your manifests, test logic\!
    
      - test environments: OS, distribution, puppet and ruby versions,
        libs, facts, parameter logic (e.g. `if $packagemanage ...`)
    
    For writing unit and integration tests for Puppet code we need to
    know some terminology:
    
      - gem  
        a ruby package (apps and libraries), list all installed with
        `gem list`
    
      - Gemfile  
        a file describing the gems the program depends on
    
      - bundle (gem install bundler)  
        installs/updates/etc the gems in the Gemfile
    
      - bundle exec  
        execute in context of the current bundle
    
      - Rakefile  
        describes how to do tasks (similar to a Makefile describing how
        to compile and link)
    
      - rake  
        execute a task that is described in the Rakefile, e.g. `bundle
        exec rake lint`
    
      - spec  
        is short for specification, a test
    
      - puppetlabs-spec-helper  
        a gem that standardizes testing tasks (the list from  
        `bundle exec rake -T`) and helps with different puppet versions
        of improves fixture downloads
    
      - fixtures  
        the environment needed to test, meaning modules are downloaded
        in this directory
    
    FORTUNATELY WITH THE PUPPET DEVELOPMENT KIT, WE DON’T HAVE TO WORRY
    ABOUT ALL THESE DETAILS ANYMORE :) (at least not for basic linting
    and unit testing...)
    
    Lets try out some testing with the [puppet-gossinbackup puppet
    module](https://bitbucket.org/ehjelm/puppet-gossinbackup)

3.  Login on a linux host where `puppet` is installed in a recent
    version, check  
    `puppet --version`

4.  Clone the puppet-gossinbackup module, rename it to `gossinbackup`
    and cd to its directory  
    `git clone https://bitbucket.org/ehjelm/puppet-gossinbackup.git`  
    `mv puppet-gossinbackup gossinbackup`

5.  **Validate syntax**
    
        puppet parser validate .

6.  **Linting (code style)** (maybe need to do `apt-get install
    puppet-lint`)
    
        puppet-lint manifests/*
        # now you can both lint and validate syntax with
        pdk validate puppet
        # similarly for your metadata (basically metadata.json) with
        pdk validate metadata
        # or everything
        pdk validate .
        # btw, you can try to autofix problems with 
        # pdk validate -a . 
    
    If you are unsure on your output is an error, warning, info or
    something else, in other words you are wondering *will this stop my
    pipeline?* you can check to see of the return value of the last
    command is different from zero with `echo $?`

7.  **Smoke test (–noop)**
    
        puppet module install puppetlabs-inifile
        puppet module install puppetlabs-vcsrepo
        puppet module install puppetlabs-stdlib
        puppet apply \
         --modulepath=/home/ubuntu:/home/ubuntu/.puppetlabs/etc/code/modules \
         --noop examples/init.pp
        # note: of course your modulepath might be different if you are not logged
        # in as the ubuntu user

8.  **Unit tests (rspec-puppet)**  
    For rspec-puppet syntax see [Naming
    conventions](https://github.com/rodjek/rspec-puppet#naming-conventions)
    (and the rest of that document). Run all unit tests with  
    `pdk test unit`  
    study briefly the files in `spec/classes/`.  
    Add a unit test to `spec/classes/gossinbackup_spec.rb` which tests
    if the number of included classes from this file is 2 ([Checking the
    number of
    resources](https://github.com/rodjek/rspec-puppet#checking-the-number-of-resources)).
    Or maybe the number of classes should be 3?

9.  **Integration and Acceptance tests**  
    This is where we test the system components together as a
    collective, typically with [ServerSpec](http://serverspec.org/) and
    [Beaker](https://github.com/puppetlabs/beaker) or
    [Litmus](https://github.com/puppetlabs/puppet_litmus). Acceptance
    tests in theory also means that we bring in “users” to verify the
    system.

10. **Deployment**  
    see r10k from last week ([stage ’r10k
    deployment’](https://github.com/CanalTP/puppet-nrpe/blob/master/Jenkinsfile))

# Monitoring

##### About this content

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/cover_practical_monitoring.jpg)
![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/cover_art_of_monitoring.jpg)

This content is heavily inspired by the excellent books: “Practical
Monitoring: Effective Strategies for the Real World” by Mike Julian  and
“The Art of Monitoring” by James Turnbull .

## What is monitoring?

##### Components

Logical view:

1.  Agents

2.  Event router / Pipeline

3.  Data storage

4.  Alerts

5.  Visualization (Dashboards)

6.  Analytics

Agents gather syslog, event logs, metrics, ...

Event routers/pipelines/filters

decision makers/alerts

Databases:

  - time-series database (investigating events)

  - log database (trends and analysis)

  - state database (is everything ok)

Visualizers:

  - Current health (“checks”)

  - Trends (metrics as time series)

  - Log analysis (logs also feed metrics)

## Anti-Patterns

##### What NOT to do

  - Silver bullet\!

  - Monitoring-as-a-job

  - For compliance reasons...

  - Alerting without fixing

  - Manual configuration

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/facepalm.png)

The following is primarily from chapter one of .

There are no silver bullets. Don’t think about buying the perfect
monitoring tool that will solve your monitoring problem. Monitoring is a
complex problem, and probably you need many tools to achieve a good
setup for your organization. Sometimes you also need to write your own
tools (many times as plugins to your monitoring tools/framework).

To be good at monitoring you need to know a bit about what you are
monitoring. Having only a specialized team of people doing monitoring
doesn’t make sense. Monitoring is a skill that most people in your
DevOps-team should have. Some are more specialists than others, and some
have special knowledge about the monitoring tools/framework itself, but
having monitoring as an integrated skill is extremely valuable.

Make your monitoring system valuable. Maybe the original reason for
introducing it was regulatory requirements (compliance, required by a
contract with a partner company), but if this remains the only reason
you have it, it will not be a very valuable system. Gathering data in a
monitoring system can be extremely valuable for many reasons
(troubleshooting, behaviour prediction, incident management, etc).
Appreciate your monitoring system and integrate it into your workflow.

It is nice to be notified when there is a problem, but it is even better
to solve problem and make the notification go away. Sometimes you might
end up adding more monitoring to an unstable application instead of
fixing the application itself. A problem doesn’t go away before it is
fixed.

Automation is a good thing. Your application and infrastructure
deployments are automated but the addition of monitoring (or adding your
application to the monitoring system) is a manual or semi-automatic
process. It is much better to have monitoring automated in the same
manner as your other infrastructure components and application: they
should self-register (service discovery or message queues), and be
automatically configured with the correct monitoring plugins and
parameters. Commonly the PubSub (Publish-Subscribe) pattern  is used in
a monitoring framework.

## Design Patterns

##### What TO DO

  - UNIX philosophy
    
    1.  data collection
    
    2.  data storage
    
    3.  visualize
    
    4.  analytics
    
    5.  alerts

  - Start from the user perspective

  - Consider buying

  - Always improve and learn

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/water.jpg)

The following is primarily from chapter two of .

The UNIX philosophy was formulated in  and its item number one is

> Make each program do one thing well. To do a new job, build afresh
> rather than complicate old programs by adding new "features".

Monitoring is a complex problem and complex problems are most easily
solvable by decomposing them into smaller subproblems. In other words,
build a monitoring system from several components and try to choose the
most suitable component for each subproblem. Follow the original UNIX
philosophy instead of trying to build or buy one big monolithic system.

  - Data collection  
    Agents can *push* data (on a regular schedule or event-based) or the
    server can *pull* data from nodes. Sometimes you don’t have a choice
    so a hybrid solution is common. The data you collect are *metrics*
    (*counter* or *gauge*) and *logs*.

  - Data storage  
    Metrics data are stored in *time-series databases* which are
    specialized databases that store timestamp-value (key-value) pairs
    (datapoints). Many time-series databases do not store the oldest
    datapoints as exact values, but use some function (averaging) to
    reduce storage requirement of older datapoints. The reason for this
    is that the exact datapoint might not be important anymore, but you
    still would like to have a indicator of a long-term trend. It is
    also common to expire data, meaning you don’t keep data forever, you
    delete data older than a specific time period. Logs are stored in
    databases that allow fast searches, e.g. elasticsearch or splunk.
    Logs come in many different formats, so it’s good practice to
    standardize logs and enrich log entries with additional information
    like geo-location or fine-grained timestamps before storing them. In
    addition to a time-series database and a log database, you also have
    a “state database” which stores the current state of everything you
    are monitoring (if things are “ok or not”).

  - Visualize  
    The most important characteristic of the visualize-component is that
    it is adaptable and flexible. It is easy to be overwhelmed with
    data. Sometimes you need to dig into details while other times you
    need the big picture. You need to see that data that is important,
    and the person in charge of the service knows what is important
    about that service and should probably design the dashboard
    components for that service. Metrics are most of the times displayed
    as line graphs so it is easy to see trends.

  - Analytics  
    Some times you need to do long-term analytics and reporting, e.g.
    for service level agreements (SLAs). If you need to accurately
    measure your downtime, you first need to decide how specific you
    should be. E.g. do you need to capture downtime that are shorter
    than ten seconds? if so you need to gather data every five second
    according to Nyquist-Shannon sampling theorem .

  - Alerts  
    Alerts are not needed for everything you are monitoring.

The most important characteristic of any IT-system is that it “actually
works”. This means that the “user” (not necessarily a physical human
being) who is interfacing with the system can use the system as
expected. We should start monitoring from this perspective. If you are
in charge of an online chat system: can users post messages? can they
read messages? thats the first thing that a monitoring system should
check. Much of this can be done by checking HTTP(S) response codes. It
is more important than checking if the CPU-load on a backend server is
0.5 or 0.7.

As mentioned, monitoring is a hard problem, it takes time to become an
expert (remember that it should be integrated, everyone should know a
bit about, but someone needs to be experts also). All that time spent
creating a small group of experts costs money. The alterntive (remember
hybrids exists, this is not binary) is of course to buy a SaaS
solution/subscription. Especially with respect to architecting all these
tools (sensu, prometheus, influxdb, grafana, ...) properly you might be
better off byuing them as SaaS (at least in the beginning).

It takes years to build a good monitoring system. During those years
your organization and your organization’s needs have probably changed.
Big changes trigger most likely architectural changes to your monitoring
system. The way to handle this is not rocket science, it’s the same
solution that you should apply to most parts of your life: always look
to improve and learn more.

## Alerts

##### Alerts

Chapter three of M. Julian :

  - Don’t send alerts to email

  - No simple thresholds

  - Always reevaluate your alerts

  - Use maintenance periods

  - Attempt self-healing before alerts

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/chital.jpg)

## Statistics

##### Averages

Be careful with statistical analysis, it might not be a normal
distribution

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/flaw_of_averages.jpg)
From [Flaw of
Averages](https://herdingcats.typepad.com/my_weblog/2015/03/flaw-of-averages.html)
(copyright Jeff Danziger)

Response times [Response Times: The 3 Important
Limits](https://www.nngroup.com/articles/response-times-3-important-limits)

Difference between response and page load times [Find out how you stack
up to new industry benchmarks for mobile page
speed](https://www.thinkwithgoogle.com/marketing-resources/data-measurement/mobile-page-speed-new-industry-benchmarks)

chp 5 KPI

chp 6,7 frontend monitoring, app monitoring

chp 8 server monitoring, load balancing etc

chp 9 net and snmp

chp 10 security monitoring, auditd, rkhunter, ossec, ids, compliance

chp 11 case and examples

## Analysis

##### Analysis

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/xkcd_correlation.png)
From [XKCD Correlation](https://xkcd.com/552/)

## Systems

### Sensu

##### Sensu

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/sensu-go-arch.png)
From [Sensu docs “Architecture
overview”](https://docs.sensu.io/sensu-go/5.14/installation/install-sensu/#architecture-overview)

[Sensu Community Plugins](https://github.com/sensu-plugins) (do you
trust community plugins? well, plugins are commonly just small scripts
(bash, python, ruby, ...) so you should manually verify them before
putting them in production).

### InfluxDB

##### InfluxDB

[InfluxDB](https://www.influxdata.com/products/influxdb-overview)

Other choices are Graphite, OpenTSDB, etc

### Grafana

##### Grafana

[Grafana](https://grafana.com)

### Elastic stack

##### Elastic/ELK stack

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/myelasticstack.png)

##### Elastic/ELK stack

![image](/home/erikhje/office/kurs/iac/08-monitoring/tex/../../00-ILLUSTRATIONS/elasticstack.png)
From [Beats and
Logstash](https://www.elastic.co/guide/en/logstash/current/deploying-and-scaling.html#_beats_and_logstash)

For centralized log management, two of the most popular choices are
Splunk and Elastic/ELK stack.

[“Unless you are using a trial license, Elastic Stack security features
require SSL/TLS encryption for the transport networking
layer.”](https://www.elastic.co/guide/en/elasticsearch/reference/current/configuring-tls-docker.html)

[Production
use](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_notes_for_production_use_and_defaults)

## Review questions and problems

1.  Describe the client and server components of a typical ELK-stack.
    What does each component do and how do they communicate? (feel free
    to illustrate with a drawing)

2.  Describe the architecture/monitoring flow of a Sensu monitoring
    system.

## Lab tutorials

1.  Make sure you have a keypair and the linux and windows security
    groups. Download or clone the git repo at  
    <https://gitlab.com/erikhje/iac-heat-a>  
    and create the stack.
    
        # environment file contents:
        parameters:
          key_name: KEY_NAME
        
        # create the stack:
        openstack stack create -e iac_top_env.yaml -t \
         iac_top.yaml iac
    
    For using the Senso Go Dashboard remember to give access to the port
    (normally you would have a reverse proxy of course)  
    
        openstack security group rule create --protocol tcp \
         --remote-ip 0.0.0.0/0 --dst-port 3000 linux
    
    Wait ca 10 minutes, login to the dashboard in `mon`, username is
    `admin` and password is in
    `/etc/puppetlabs/code/shared-hieradata/common.yaml`
    
    Study the Puppet-code in `backend.pp`, `agent_linux.pp` and
    `agent_windows.pp` so you understand how Sensu work wrt PubSub
    pattern.
    
    Note: we are waiting for a fix [Support defining agent and backend
    service environment
    variables](https://github.com/sensu/sensu-puppet/pull/1160), so for
    now we need to define full path to all check commands.

2.  Study the code in [Puppet
    ELK](https://github.com/githubgossin/control-repo-cr/tree/production/site/profile/manifests/elk)
    to see how you would typically deploy and elastic stack (ELK).

3.  Maybe opening the port 3000 to access Sensu GUI was a bad choice,
    try to implement a reverse proxy on port 80 and/or 443 instead (you
    should find the Puppet code for that in the ELK-code linked to
    above).

# Container Orchestration

## Background

Google’s decade of experience with Borg \(\Rightarrow\) Kubernetes
[Large-scale cluster management at Google with
Borg](https://ai.google/research/pubs/pub43438)

## Architecture

Kubernetes architecture: two figures at [Concepts Underlying the Cloud
Controller
Manager](https://kubernetes.io/docs/concepts/architecture/cloud-controller)

### Master/Controller

  - etcd  
    distributed key-Value store, [etcd](https://etcd.io)  
    "everything Kubernetes needs to store"

  - kube-apiserver

  - kube-scheduler  
    (bin packing "like Tetris")

  - kube-controller-manager  
    runs controllers (most of them compiled into itself)  
    [kube-controller-manager](https://kubernetes.io/docs/concepts/overview/components/#kube-controller-manager)

  - (cloud-controller-manager)

### Node/Worker

  - kubelet  
    [kubelet](https://kubernetes.io/docs/concepts/overview/components/#kubelet)

  - kube-proxy  
    network, connection forwarding

  - container runtime (Docker)

### Kubernetes Objects (for Devs)

("for the developers using Kubernetes")

"Kubernetes Objects are persistent entities in the Kubernetes system"  
[Understanding Kubernetes
Objects](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/#understanding-kubernetes-objects)

Read about objects in "Resource Categories" at [API
OVERVIEW](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16)

Objects are described in a yaml-file, kubectl convert to json when
calling the api-server, Kubernetes job is to match the objects actual
state to the desired state in the yaml-file (match the "Object spec" to
the "Object status")

  - Pod  
    One or more containers, running as a unit on one node, smallest unit
    of deployment [Understanding
    Pods](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/#understanding-pods)

  - (Replicasets)  
    we dont use them directly, they create and destroy Pods dynamically

  - Deployment  
    read and see example at  
    [Creating a
    Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#creating-a-deployment)

  - Service  
    How to access your deployment  
    [Defining a
    Service](https://kubernetes.io/docs/concepts/services-networking/service/#defining-a-service)  
    ClusterIP, NodePort, LoadBalancer or ExternalName  
    [Publishing Services
    (ServiceTypes)](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)

Should probably study [Helm](https://v3.helm.sh) and [Cloud Native
Application Bundle](https://cnab.io) also.

And a good popular book is [Cloud Native DevOps with
Kubernetes](https://www.oreilly.com/library/view/cloud-native-devops/9781492040750/).

## Review questions and problems

1.  What is a Kubernetes Object? How is a Kubernetes object
    described/represented?

2.  Which Kubernetes components are present on a Node/Worker?

3.  What is a Kubernetes Service? What is a Deployment? describe the
    relationship between a Service and a Deployment.

## Lab tutorials

1.  Start the [Kubernetes lab
    exercise](https://github.com/githubgossin/IaC-heat-k8s) (remember to
    edit `iac_top_env.yaml` first), wait for about ten minutes, log in
    on `manager` and wait until you see all hosts (all controllers and
    workers) in DNS:  
    
        $ cat /var/lib/bind/zones/db.borg.trek
        ;
        ; BIND data file for borg.trek zone.
        ; File managed by puppet.
        ;
        $ORIGIN borg.trek.
        $TTL    604800
        @       IN      SOA     manager.borg.trek. admin.borg.trek. (
                        1540728390              ; Serial
                        604800          ; Refresh
                        86400           ; Retry
                        2419200         ; Expire
                        604800  )       ; Negative Cache TTL
        ;
        @               IN      NS      manager.
        ;
        controller-0            IN      A       192.168.180.105
        controller-1            IN      A       192.168.180.117
        controller-2            IN      A       192.168.180.115
        manager         IN      A       192.168.180.103
        test            IN      A       192.168.180.111
        worker-0                IN      A       192.168.180.110
        worker-1                IN      A       192.168.180.102
        worker-2                IN      A       192.168.180.114

2.  Put your OpenStack service account information in `cloud.conf`, and
    run the script to create all keys and site-specific data:
    
        cd /etc/puppetlabs/code/environments/production
        vi site/profile/files/etc/kubernetes/cloud.conf
        bash create_k8s_config.bash

3.  Wait five minutes, the log in to one of the controllers and get to
    know a Kubernetes controller (nice
    [Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet)):
    
      - Check the etcd cluster:
        
            # create a file ETCD with contents (replace IP-addresses with your own):
            export ETCDCTL_API=3
            export ETCDCTL_ENDPOINTS="https://192.168.180.105:2379,\
            https://192.168.180.115:2379,https://192.168.180.117:2379"
            export ETCDCTL_CACERT="/etc/kubernetes/pki/etcd/ca.crt"
            export ETCDCTL_CERT="/etc/kubernetes/pki/etcd/client.crt"
            export ETCDCTL_KEY="/etc/kubernetes/pki/etcd/client.key"
            # source the file and check health (don't worry about deprecated-messages):
            . ETCD
            etcdctl member list
            etcdctl endpoint health
    
      - See that you have certificates that your CLI-programs can use to
        communicate with the API-server, and set environment variable:
        
            kubeadm # does not work
            kubectl # does not work
            cat /etc/kubernetes/admin.conf
            export KUBECONFIG=/etc/kubernetes/admin.conf
            kubectl get componentstatuses 
            # ask one of the other controllers:
            kubectl get componentstatuses -s https://IP_OF_A_CONTROLLER:6443
    
      - kubeadm (the Puppet module uses kubeadm to deploy the cluster)
        
            kubeadm config -h
            kubeadm config view | less
    
      - kubectl (is your main interface to Kubernetes)
        
            kubectl help | less
            kubectl explain
            kubectl explain node
            kubectl describe
            kubectl describe node
            kubectl get node
            kubectl get namespaces          # or just 'get ns'
            kubectl get pod
            kubectl get pod -n kube-system  # namespace kube-system
            kubectl get pods --all-namespaces --output wide
            kubectl get pods -o wide --all-namespaces | grep controller-0
            docker ps | grep pause
            # "In Kubernetes, the pause container serves as the "parent container" 
            # for all of the containers in your pod. The pause container has two 
            # core responsibilities. First, it serves as the basis of Linux 
            # namespace sharing in the pod. And second, with PID (process ID) 
            # namespace sharing enabled, it serves as PID 1 for each pod and reaps 
            # zombie processes." https://www.ianlewis.org/en/almighty-pause-container
            kubectl get deployment
            kubectl get deployment -n kube-system
            kubectl get svc
            kubectl get svc -n kube-system
            kubectl describe svc kube-dns -n kube-system
            kubectl help get                # or kubectl get -h
            kubectl api-versions # show all api versions
            # kubectl api-resources in newer Kubernetes
            # would be nice to list all object types supported,
            # but havent figured that one out yet ('kind:' in the yaml file)
    
      - Debugging/Troubleshooting
        
            # if running
            kubectl logs -f -n kube-system kube-apiserver-controller-0 
            # if not
            docker ps -a # see also exited containers
            docker ps -a | grep k8s_kube-proxy | awk '{print $1}' | xargs docker logs -f
            journalctl -f -u kubelet
    
      - `systemctl status | less` (search for docker and kubelet to see
        that all k8s-services runs in containers except kubelet)
        
            systemctl status kube... # PRESS TAB

4.  Do (and read the text) the entire lab [Run a Stateless Application
    Using a
    Deployment](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment),
    in addition you might find these two commands useful:
    
        # open a separate window on one the controllers and watch what happens with
        kubectl get events --watch
        # view extra info (e.g. which nodes the pods are running on) with
        kubectl get pods -l app=nginx --output wide

5.  Let’s see if we can integrate with OpenStack, see [Cloud
    Providers](https://kubernetes.io/docs/concepts/cluster-administration/cloud-providers)
    (this is for in-tree cloud providers, in the future this will be
    separated into its own [Cloud Controller
    Manager](https://kubernetes.io/docs/concepts/architecture/cloud-controller)).
    
      - Create the file `v1.16.2` with the content in the gray box at
        [kubeadm](https://kubernetes.io/docs/concepts/cluster-administration/cloud-providers/#kubeadm).
    
      - `kubeadm upgrade apply -f v1.16.2` and see changes (grep for
        cloud, and see timestamps with `ls -ltr`) in  
        `/etc/kubernetes/manifests` and `/etc/systemd/system/kube*`
    
      - And your cluster is now broken... (check logs\!)

6.  Delete the stack. Change the `iac_rest.yaml` to create two
    controllers and four workers. Start the stack, create the Kubernetes
    cluster and check that you get two controllers and four workers.

# Universal Scalability Law

## Scalability

We need to show some theory, sysadm is not just practice (we can do
fancy stuff with monitoring data of course), when doing this at the
MSc/PhD-level look to
[TTM4110](https://www.ntnu.no/studier/emner/TTM4110) and
[TTM4158](https://www.ntnu.no/studier/emner/TTM4158).

We can parallellize

  - Puppetservers

  - Sensu backends

  - elasticsearch and logstash for ELK

  - everything in Kubernetes...

  - CPUs per OS

  - nodes/drives in a Ceph cluster

  - etc

  - etc

*What is scalability? It’s a function\! The USL is a mathematical
definition of scalability. (it gives us a measure of capacity of a
system)*

[VelocityConf Demo: Universal Scalability
Law](https://www.desmos.com/calculator/3cycsgdl0b)

How is the system throughput if we increase load? (the claim is that
many systems are close to linear scaling, so let’s test a system that
has a bunch of parallell nodes or dynamically autoscales as load
increases, does it scale nicely to increased load?)

## Universal Scalability Law

\(N\): number of nodes (or something else, see 1.3 on why Universal)
(e.g. number of elasticsearch instances, known number)

\(X(N)\): system throughput / speedup / capacity (e.g. queries per
second, computed)

\(\lambda\): throughput for one node (e.g. 1000 queries per second
parameter, measured number)

\(\sigma\): contention (scatter-gather (planning/scheduling,merging
results)) (e.g. contention parameter, estimated)

\(\kappa\): coherency (crosstalk,coordinate) (e.g. coherency parameter,
estimated)

\[X(N)=\frac{\lambda N}{1+\sigma(N-1)+\kappa N(N-1)}\]

(can be applied to both speedup in latency and increase in load)

Why this formula???  
\- because we can predict how the system behaves when scaled further...

## Review questions and problems

1.  Why is scalability a function and not a number?

2.  What is the purpose of the Universal Scalability Law? How do you use
    it?

## Lab tutorials

1.  Let’s download a Python implementation and play with it.
    
        # don't be root
        git clone https://github.com/mookerji/sca_tools.git
        cd sca_tools
        sudo apt-get install python python-pip
        ./setup.py install --user
        pip install python-dateutil pytz click matplotlib lmfit
        # https://www.spec.org/sdm91
        # The benchmark simulates a number of users working on 
        # a UNIX server (editing files, compiling .. . ) and
        # measures the number of script executions per hour
        less less fixtures/specsdm91.csv 
        python sca_tools/sca_fit.py --model_type usl fixtures/specsdm91.csv
        # view the plots, the png-files generated in fixtures:
        eog fixtures/*png # WHICH PLOT IS THE USL-fit?

2.  Can we measure something to generate our own dataset?
